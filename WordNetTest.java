/**
 * Created with IntelliJ IDEA.
 * User: sthangalapally
 * Date: 5/22/13
 * Time: 11:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class WordNetTest {


    public static void main(String[] args) {
        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length   = sap.length(v, w);
            int ancestor = sap.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}

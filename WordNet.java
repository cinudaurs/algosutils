/**
 * Created with IntelliJ IDEA.
 * User: sthangalapally
 * Date: 5/23/13
 * Time: 12:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class WordNet {

   private SET<String> nounSet = new SET<String>();

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms){}

    // the set of nouns (no duplicates), returned as an Iterable
    public Iterable<String> nouns(){

        return nounSet;


    }

    // is the word a WordNet noun?
    public boolean isNoun(String word){

        return true;

    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB){

        return -1;

    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
// in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB){



        return null;

    }

    // for unit testing of this class
    public static void main(String[] args){}

}

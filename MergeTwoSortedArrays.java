/**
 * Created with IntelliJ IDEA.
 * User: sthangalapally
 * Date: 5/13/13
 * Time: 8:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class MergeTwoSortedArrays {

/* Input: a[], b[], n (number of elements in a) and
* m (number of elements in b)   */

     public static int m;
     public static int n;


    public static int[] MergeArrays(int[] a, int[] b, int m, int n)
    {


        int k = m + n - 1; // Index of last location of array b
        int i = m - 1; // Index of last element in array a
        int j = n - 1; // Index of last element in array b
// Start comparing from the last element and merge a and b

        while (i >= 0 && j >= 0) {
            if (a[i] > b[j]) {
                a[k--] = a[i--];
            } else {
                a[k--] = b[j--];
            }
        }
        while (j >= 0) {
            a[k--] = b[j--];
        }
                      return a;
    }



    public static void main(String[] args)
    {
          int k = StdIn.readInt();
          int m = StdIn.readInt();
          int n = StdIn.readInt();

          int[] a = StdArrayIO.readInt1D(k,m);
          int[] b = StdArrayIO.readInt1D(n);

        int[] c = MergeArrays(a,b,m,n);


        StdArrayIO.print(c);
       // StdArrayIO.print(b);
        StdOut.println();
    }


}

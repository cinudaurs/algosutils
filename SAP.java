/**
 * Created with IntelliJ IDEA.
 * User: sthangalapally
 * Date: 5/23/13
 * Time: 12:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class SAP {

    public SAP(Digraph G){

    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w){



        return -1;
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w){
        return -1;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w){
        return -1;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w){
        return -1;
    }

    // for unit testing of this class (such as the one below)
    public static void main(String[] args){

    }

}
